﻿using System;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.CommandLineUtils;

namespace Sink
{
    class Sink
    {
        static string helpOptions = "-?|-h|--help";
        static CommandLineApplication app;

        static void Main(string[] args)
        {
            var config = GetAwsConfig();

            // If we haven't already set the env variables for our AWS creds,
            // set them.
            if (Environment.GetEnvironmentVariable("AWS_ACCESS_KEY_ID") == null)
                Environment.SetEnvironmentVariable("AWS_ACCESS_KEY_ID", config.awsAccessKey);
            if (Environment.GetEnvironmentVariable("AWS_SECRET_ACCESS_KEY") == null)
                Environment.SetEnvironmentVariable("AWS_SECRET_ACCESS_KEY", config.awsSecret);

            // vvv ALL OF THIS IS CLUTTERING MY `Main` method! Please refactor
            //     it off of my lawn!

            app = new CommandLineApplication();
            app.Name = "sink";
            app.HelpOption(helpOptions);

            app.Command("store", (command) => {
                Store.Configure(command, config);
            });

            app.Command("retrieve", (command) => {
                Retrieve.Configure(command, config);
            });

            // TODO: Make *something* the default
            app.OnExecute(() => {
                Console.WriteLine("Sink: Run with `{0}` for help", helpOptions);
                return 0;
            });

            app.Execute(args);
        }

        static AwsConfig GetAwsConfig()
        {
            AwsConfig config = new AwsConfig();
            string configLocation = GetAwsConfigLocation();

            if (configLocation == null || !File.Exists(configLocation)) {
                Console.WriteLine("Couldn't find aws config: aws-config.json");
                return config;
            }

            try
            {
                using (StreamReader file = File.OpenText(configLocation))
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    var o = (JObject)JToken.ReadFrom(reader);
                    config = new AwsConfig {
                        awsAccessKey = (string)o.GetValue("access_key_id"),
                        awsSecret = (string)o.GetValue("secret_access_key"),
                        region = (string)o.GetValue("region"),
                        bucket = (string)o.GetValue("defaultBucket")
                    };
                }
            }
            catch (System.IO.FileNotFoundException e)
            {
                Console.WriteLine("Couldn't find aws config: aws-config.json");
                Console.WriteLine(e.Message, e.InnerException);
            }

            return config;
        }

        static string GetAwsConfigLocation()
        {
            string configLocation = @"./aws-config.json";

            if (File.Exists(configLocation))
                return configLocation;

            configLocation = Path.GetDirectoryName(
                System.Reflection.Assembly.GetEntryAssembly().CodeBase) + "/aws-config.json";

            if (File.Exists(configLocation))
                return configLocation;

            configLocation = Path.GetDirectoryName(
                System.Reflection.Assembly.GetEntryAssembly().Location) + "/aws-config.json";

            if (File.Exists(configLocation))
                return configLocation;

            configLocation = "~/aws-config.json";

            if (File.Exists(configLocation))
                return configLocation;

            configLocation = "~/.aws/aws-config.json";

            if (File.Exists(configLocation))
                return configLocation;

            return null;
        }
    }
}
