using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Amazon.S3;
using Amazon.S3.Transfer;
using Microsoft.Extensions.CommandLineUtils;

// TODO: Add a failsafe to protect local files from getting overwritten

namespace Sink
{
    public class Retrieve
    {
        public CommandLineApplication command;
        static string helpOptions = "-?|-h|--help";

        public CommandArgument filesArgument;
        public CommandOption bucketNameOption;
        public CommandOption outputOption;
        public CommandOption outputDirOption;
        public CommandOption directoryOption;
        public CommandOption stdoutOption;
        public CommandOption verbosityOption;

        public string awsRegion;
        public string defaultBucket;

        public static void Configure(CommandLineApplication retrieveCommand, AwsConfig config)
        {
            retrieveCommand.Description = "Take ya stuff out of the sink.";
            retrieveCommand.HelpOption(helpOptions);

            var filesArgument = retrieveCommand.Argument("[files...]",
                "The items to retrieve", true);

            var bucketNameOption = retrieveCommand.Option(
                "-b|--bucket <bucketName>",
                "S3 Bucket to upload to",
                CommandOptionType.SingleValue);

            var outputOption = retrieveCommand.Option(
                "-o|--output <outputLocation>",
                "Where to put the retrieved item(s)",
                CommandOptionType.SingleValue);

            var outputDirOption = retrieveCommand.Option(
                "-d|--outputDir <outputDirectory>",
                "Folder in which to put output item(s)",
                CommandOptionType.SingleValue);

            // Doesn't work with directories, only works with single files
            var stdoutOption = retrieveCommand.Option(
                "-s|--stdout",
                "Whether or not to output to stdout instead of a file",
                CommandOptionType.NoValue);

            var directoryOption = retrieveCommand.Option(
                "-D|--directory",
                "Is the S3 object to retrieve a directory?",
                CommandOptionType.NoValue);

            var verbosityOption = retrieveCommand.Option(
                "-v|--verbose",
                "S3 Bucket to upload to",
                CommandOptionType.NoValue);

            retrieveCommand.OnExecute(() => {
                // This seems dumb
                (new Retrieve() {
                    command = retrieveCommand,
                    filesArgument = filesArgument,
                    bucketNameOption = bucketNameOption,
                    outputOption = outputOption,
                    outputDirOption = outputDirOption,
                    directoryOption = directoryOption,
                    stdoutOption = stdoutOption,
                    verbosityOption = verbosityOption,

                    defaultBucket = config.bucket,
                    awsRegion = config.region
                }).Run();
                return 0;
            });
        }

        public void Run()
        {
            string bucketName = bucketNameOption.HasValue() ?
                bucketNameOption.Value() :
                defaultBucket;

            var regionEndpoint = Amazon.RegionEndpoint.GetBySystemName(
                    awsRegion != null ?  awsRegion : "us-east-1");

            if (filesArgument.Values.Count > 0) {
                List<Task> retrievalTasks;

                using (var amazonClient = new AmazonS3Client(regionEndpoint))
                {
                    if (this.stdoutOption.HasValue())
                        retrievalTasks = this.ProcessToStdOut(amazonClient, bucketName);
                    else
                        retrievalTasks = this.ProcessToFile(amazonClient, bucketName);

                    // This is basically where the buck stops, so we have to
                    // wait for all of our async tasks to finish here
                    Task.WhenAll(retrievalTasks).Wait();
                }
            }
        }

        private List<Task> ProcessToFile(AmazonS3Client amazonClient, string bucketName)
        {
            var retrievalTasks = new List<Task>();

            using (var transferUtility = new TransferUtility(amazonClient))
            {
                filesArgument.Values.ForEach((keyName) => {
                    if (verbosityOption.HasValue())
                        Console.WriteLine("Processing Key: {0}", keyName);

                    var fileName = this.getFileName(keyName);
                    var location = this.getLocation();

                    if (directoryOption.HasValue()) {
                        var request = new TransferUtilityDownloadDirectoryRequest {
                            BucketName = bucketName,
                            LocalDirectory = location + fileName,
                            S3Directory = keyName,
                        };

                        retrievalTasks.Add(
                            ProcessTransfer(transferUtility, request)
                        );
                    } else {
                        var request = new TransferUtilityDownloadRequest {
                            BucketName = bucketName,
                            Key = keyName,
                            FilePath = location + fileName,
                        };

                        retrievalTasks.Add(
                            ProcessTransfer(transferUtility, request)
                        );
                    }
                });
            }

            return retrievalTasks;
        }

        // This is a dumb name, change it to make it more sensible.
        async private Task ProcessTransfer(TransferUtility transferUtility, TransferUtilityDownloadDirectoryRequest request)
        {
            try
            {
                if (verbosityOption.HasValue())
                    Console.WriteLine("[ ] Retrieving Directory: {0}...", request.S3Directory);

                await transferUtility.DownloadDirectoryAsync(request);

                if (verbosityOption.HasValue())
                    Console.WriteLine("[x] Retrieving Directory: {0}, finished", request.S3Directory);
            }
            catch (Amazon.S3.AmazonS3Exception s3Exception)
            {
                Console.WriteLine(s3Exception.Message);
            }
        }

        async private Task ProcessTransfer(TransferUtility transferUtility, TransferUtilityDownloadRequest request)
        {
            try
            {
                // All of these `if verbose` etcs. Are dumb, consider adding a
                // method that only logs when verbosity is set.
                if (verbosityOption.HasValue())
                    Console.WriteLine("[ ] Retrieving Directory: {0}...", request.Key);

                await transferUtility.DownloadAsync(request);

                if (verbosityOption.HasValue())
                    Console.WriteLine("[x] Retrieving Directory: {0}, finished", request.Key);
            }
            catch (Amazon.S3.AmazonS3Exception s3Exception)
            {
                if (s3Exception.Message.Contains("key does not exist")) {
                    Console.WriteLine("{0} If trying to retrieve a folder, add `-D`", s3Exception.Message);
                } else {
                    Console.WriteLine(s3Exception.Message);
                }
            }
        }

        // This is cool, because we can then pipe the output to something else
        // $ sink restore file.name | grep foo
        private List<Task> ProcessToStdOut(
            AmazonS3Client amazonClient, string bucketName)
        {
            var retrievalTasks = new List<Task>();

            filesArgument.Values.ForEach((keyName) => {
                var request = new Amazon.S3.Model.GetObjectRequest {
                    BucketName = bucketName,
                    Key = keyName,
                };

                retrievalTasks.Add(
                    this.ProcessFileRetrieval(amazonClient, request)
                );
            });

            return retrievalTasks;
        }

        async private Task ProcessFileRetrieval(
            AmazonS3Client client, Amazon.S3.Model.GetObjectRequest request)
        {
            // I have a hunch that the async shit in here is le fuckered.
            // NOTE: Everything *is* working fine though, so look into le
            //       fuckering later.

            try
            {
                using (var response = await client.GetObjectAsync(request))
                using (var responseStream = response.ResponseStream)
                using (var reader = new StreamReader(responseStream))
                {
                    string body  = reader.ReadToEnd();
                    Console.WriteLine(body);
                }
            }
            catch (Amazon.S3.AmazonS3Exception s3Exception)
            {
                Console.WriteLine(s3Exception.Message);
            }
        }

        // TODO: Make this function more generic... same with the other.
        private string getLocation(string fileName = "")
        {
            var location = outputDirOption.HasValue() ? outputDirOption.Value() : "./";

            if (location.Substring(location.Length - 1, 1) != "/")
                location += "/";

            return location + fileName;
        }

        private string getFileName(string keyName = "")
        {
            var fileName = outputOption.HasValue() ? outputOption.Value() : keyName;
            fileName = new DirectoryInfo(fileName).Name;
            return fileName;
        }
    }
}
