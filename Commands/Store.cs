using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using Amazon.S3;
using Amazon.S3.Transfer;
using Microsoft.Extensions.CommandLineUtils;

// TODO: Add a common interface

// TODO: (optional) support to pack things into a zip before storing
// TODO: (optional) generate alias for easy item retrieval
// TODO: (optional) support for more complex file search to upload (dir/**/*.txt)
// TODO: (optional) support for watching directories/files for changes

namespace Sink
{
    public class Store
    {
        public CommandLineApplication command;
        static string helpOptions = "-?|-h|--help";

        public CommandArgument filesArgument;
        public CommandOption bucketNameOption;
        public CommandOption directoryOption;
        public CommandOption verbosityOption;

        public string awsRegion;
        public string defaultBucket;

        public static void Configure(CommandLineApplication storeCommand, AwsConfig config)
        {
            storeCommand.Description = "Put ya stuff in the sink.";
            storeCommand.HelpOption(helpOptions);

            var filesArgument = storeCommand.Argument("[files...]",
                "The items to upload", true);

            var bucketNameOption = storeCommand.Option(
                "-b|--bucket <bucketName>",
                "S3 Bucket to upload to",
                CommandOptionType.SingleValue);

            var directoryOption = storeCommand.Option(
                "-d|--dir <targetDirectory>",
                "S3 folder to put the file in",
                CommandOptionType.SingleValue);

            var verbosityOption = storeCommand.Option(
                "-v|--verbose",
                "S3 Bucket to upload to",
                CommandOptionType.NoValue);

            storeCommand.OnExecute(() => {
                // This also seems dumb, idk
                (new Store(storeCommand) {
                    filesArgument = filesArgument,
                    bucketNameOption = bucketNameOption,
                    directoryOption = directoryOption,
                    verbosityOption = verbosityOption,
                    awsRegion = config.region,
                    defaultBucket = config.bucket,
                }).Run();
                return 0;
            });
        }

        public Store(CommandLineApplication storeCommand)
        {
            // We have all these fields that we're setting, it's debatable
            // whatever is the correct place to set them. (*______* )
            //
            // Also, this constructor was deleted as uneeded in `Retrieve.cs`
            // is it needed? Not really. Is it better to have it? Only the
            // stars can tell...
            command = storeCommand;
        }

        public void Run() {
            string bucketName = bucketNameOption.HasValue() ?
                    bucketNameOption.Value() :
                    defaultBucket;

            var regionEndpoint = Amazon.RegionEndpoint.GetBySystemName(
                    awsRegion != null ?  awsRegion : "us-east-1");

            if (filesArgument.Values.Count > 0) {
                var uploadTasks = new List<Task>();

                using (var amazonClient = new AmazonS3Client(regionEndpoint))
                using (var transferUtility = new TransferUtility(amazonClient))
                {
                    filesArgument.Values.ForEach((location) => {
                        if (verbosityOption.HasValue())
                            Console.WriteLine("Processing Key: {0}", location);

                        uploadTasks.Add(
                            ProcessLocationStore(location, bucketName, transferUtility)
                        );
                    });

                    Task.WhenAll(uploadTasks).Wait();
                }
            }
        }

        async public Task ProcessLocationStore(
            string location, string bucketName, TransferUtility transferUtility
        ) {
            var locationAttributes = File.GetAttributes(location);

            if (locationAttributes.HasFlag(FileAttributes.Directory)) {
                string dirName = new DirectoryInfo(location).Name;

                if (verbosityOption.HasValue())
                    Console.WriteLine("[ ] Uploading Directory: {0}...", location);

                var request = new TransferUtilityUploadDirectoryRequest
                {
                    Directory = location,
                    BucketName = bucketName,
                    SearchOption = SearchOption.AllDirectories,

                    // Makes sure that things are placed in proper folders
                    KeyPrefix = dirName + "/"
                };

                await transferUtility.UploadDirectoryAsync(request);

                if (verbosityOption.HasValue())
                    Console.WriteLine("[x] Uploading Directory: {0}, finished", location);
            } else {
                if (verbosityOption.HasValue())
                    Console.WriteLine("[ ] Uploading File: {0}...", location);

                var key = Path.GetFileName(location);
                if (directoryOption.HasValue())
                    key = directoryOption.Value() + "/" + key;

                await transferUtility.UploadAsync(location, bucketName, key);

                if (verbosityOption.HasValue())
                    Console.WriteLine("[x] Uploading File: {0}, finished", location);
            }
        }
    }
}
