namespace Sink
{
    public class AwsConfig
    {
        public string awsAccessKey;
        public string awsSecret;
        public string region;
        public string bucket;
    }
}