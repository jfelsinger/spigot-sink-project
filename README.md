# How to Compile

You can try getting it working with .net core. Or you can use the zipped
`Sink.zip` .net framework project.

Good luck!



# How To Use

Make sure that a configured `aws-config.json` file is present wherever you're
running the command from, or present at the executable's own location.

There is an already compiled, self-contained `.exe`, `Sink.exe` I hope it works.
Get help:

```
Sink.exe -h
```

Store an item:

```
Sink.exe store some-file.txt some-dir
```

Retrieve an item:

```
Sink.exe retrieve some-file.txt
Sink.exe retrieve some-dir -D
```
